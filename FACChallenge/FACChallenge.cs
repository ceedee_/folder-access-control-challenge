﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FACChallenge
{
    public partial class FACChallenge : Form
    {
        public FACChallenge()
        {
            InitializeComponent();
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            var publicDesktopPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonDesktopDirectory), "PublicDesktopPath");
            var userDesktopPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), "UserDesktopPath");
            var publicDocuments = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments), "PublicDocuments");
            var userDocuments = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "UserDocuments");

            tryCreateDir(publicDesktopPath);
            tryCreateDir(userDesktopPath);
            tryCreateDir(publicDocuments);
            tryCreateDir(userDocuments);

            tryRemoveDir(publicDesktopPath);
            tryRemoveDir(userDesktopPath);
            tryRemoveDir(publicDocuments);
            tryRemoveDir(userDocuments);
        }
        private void tryCreateDir(String directory)
        {
            if (Directory.Exists(directory))
            {
                this.listBoxDirectories.Items.Add("directory exists: " + directory);
            }
            else
            {
                try
                {
                    Directory.CreateDirectory(directory);
                    this.listBoxDirectories.Items.Add("success creating: " + directory);
                }
                catch (Exception e)
                {
                    this.listBoxDirectories.Items.Add("failure creating: " + directory);
                }
            }
        }

        private void tryRemoveDir(String directory)
        {
            try
            {
                Directory.Delete(directory);
                this.listBoxDirectories.Items.Add("success deleting: " + directory);
            }
            catch (Exception e)
            {
                this.listBoxDirectories.Items.Add("failure deleting: " + directory);
            }
        }

    }
}
