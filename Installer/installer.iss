; Configure signtool in InnoSetup -> Tools -> Configure Sign Tools
; and use the following command line for creating a new entry with name "signtool":
;
;   [repo-path]\Installer\sign.bat $f super-secret-password [full-filename-of-certificate.pfx]
;
; Make sure to replace the password and the path to the pfx file!

[Setup]
SignTool=signtool
AppName=Folder Access Control Challenge
AppVersion=1.0
AppPublisher=Challenger
AppPublisherURL=http://www.challenger.org/
DefaultDirName={pf}\Folder Access Control Challenge
DefaultGroupName=Folder Access Control Challenge
UninstallDisplayIcon={app}\FACChallenge.exe
Compression=lzma2
SolidCompression=yes
OutputDir=.

[Files]
Source: "..\FACChallenge\bin\Release\FACChallenge.exe"; DestDir: "{app}"; Flags: sign
Source: "..\FACChallenge\bin\Release\FAC*.*"; DestDir: "{app}"

[Icons]
Name: "{commondesktop}\My Program"; Filename: "{app}\FACChallenge.exe"
Name: "{group}\Folder Access Control Challenge"; Filename: "{app}\FACChallenge.exe"
