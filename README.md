How To: Installer building
==========================
1. Build release version of FACChallenge.sln with Visual Studio 2015 (or higher).
2. Install InnoSetup 5.6.1 (or higher).
3. Acquire a signing certificate in form of a PFX file.
4. Configure SignTool as described in Installer\installer.iss.
5. Open installer.iss with InnoSetup
6. Compile the installer.


Reproduction: Folder Access Control fail
========================================
1. Activate Folder Access Control in Windows Defender settings, confirm UAC dialog for changing this setting.
2. Install the built installer.

This will result in:

1. setup will fail to create a desktop icon
2. application will fail to create folders protected by Folder Access Control
3. a notification appears in the message center, that the access has been blocked
